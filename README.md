# Muni Pileup

__Welcome to the MUNI Pileup project!__

We are analyzing historical MUNI bus data to determine the impact on bus bunching and working on methods to help to predict and combat it. This repository is divided into the following directories:

* app - the flask app for visualizing historical and real-time bus bunching.
* data - documentation and some scripts used to pull the data. Data generally not included here due to size.
* paper - LaTeX and related files for the research paper on bus bunching.
* scripts - scripts used to process the data and run analyses not integrated into the app.





