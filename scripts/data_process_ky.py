#!/usr/bin/env python2.7

#############################
# join the AVL with MUNI data
# Note the file ends with ^M
# need to replace ^M with /r using vim:
# vim -c "%s/PREDICTABLE/PREDICTABLE\r"
# vim -c ":%s/^M//g"
#############################

import numpy as np
import pandas as pd
import time
import os
import folium

# Step 1: import AVL data #########

_load_path = "/Users/kaiyin/Documents/myGit/muni_myFolder/AVL_data"
_load_file_name = "sfmtaAVLRawData01062016.csv"

start_time = time.time()
print("importing...{0}".format(_load_file_name))
_file_full_path = os.path.join(_load_path, _load_file_name)
print("full path is : {0}".format(_file_full_path))
_AVL_data = pd.read_csv(_file_full_path)
end_time = time.time()

# To merge AVL with GTFS data, changee 'TRAIN_ASSIGNMENT' to 'block_id'
_AVL_data.rename(columns={'TRAIN_ASSIGNMENT': 'block_id'}, inplace=True)

# Step 2: import GTFS data ##########

_load_path = "/Users/kaiyin/Documents/myGit/muni_myFolder/gtfs_05_2016"

_load_file_name = "trips.txt"
_file_full_path = os.path.join(_load_path, _load_file_name)
_trips_data = pd.read_csv(_file_full_path)

# check how many NaN in block_id?
ind = pd.isnull(_trips_data['block_id'])
nan_block_id = _trips_data.loc[ind, ]
print("Trips data has dimension: {0}, the # of NaN for block_id:{1}\
      ".format(_trips_data.shape, nan_block_id.shape[0]))


_load_file_name = "routes.txt"
_file_full_path = os.path.join(_load_path, _load_file_name)
_route_data = pd.read_csv(_file_full_path)

# Step 3: Pre-processing data

# 3.1. merge trips and routes data
_trip_rt_dt = pd.merge(_trips_data, _route_data, how='left',
                       on=['route_id'], suffixes=['-l', '-r'])

print("The trips data has dimension: {0}; the route data has\
      dimension: {1}; The merged trip_route data: {2}".format(
      _trips_data.shape, _route_data.shape, _trip_rt_dt.shape))

# ###### ####### #######
# 3.2 Each route may relate to multiple block_id!

gp_route = _trip_rt_dt.groupby(['route_id'])
gp_route_agg = gp_route.agg({"block_id": pd.Series.nunique,
                            "trip_id": pd.Series.nunique})
print("The groups by route_id is:\n".format(gp_route.groups))

print(gp_route_agg.loc[gp_route_agg['block_id'] == 1, ].head())
print(gp_route_agg.loc[gp_route_agg['block_id'] > 1, ].head())

# ####### Show Map
SF_COORDINATES = (37.78318, -122.44625)

route_check = _AVL_data.loc[_AVL_data['block_id'] == 101, ]

map_route_check = folium.Map(location=SF_COORDINATES,
                             zoom_start=12)

for i in range(route_check.shape[0]):
    long_id = route_check.iloc[i, ]['LONGITUDE']
    lat_id = route_check.iloc[i, ]['LATITUDE']
    folium.Marker([lat_id, long_id],
                  icon=folium.Icon(icon='green')
                  ).add_to(map_route_check)

map_route_check.create_map(path='route.html')
