"""
	using sqlite3 in python for now
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('postgresql://gis:gis@localhost/gis', echo=True)
db_session = scoped_session(sessionmaker(autcommit=False,
										 autoflush=False,
										 bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
	#import pileup.models
	Base.metadata.create_all(bind=engine)
	
"""



"""JUST USING PANDAS TO MAKE THE TABLES RIGHT NOW"""
def init_db():
	import sqlite3 as lite
	import sys
	
	conn = lite.connect('E:/Kaggle/Muni/munipileup/pileup.db')
	
	c = conn.cursor()
	
	c.execute('CREATE TABLE avldata (date text, trans text)')

	busdata = []
	c.executemany('INSERT INTO avldata VALUES (?,?,?,?,?,?,?,?,?,?,?)', busdata)

	conn.commit()
	conn.close()


def fill_db():



