from __future__ import absolute_import
from celery import Celery 

app = Celery('downloadtask', broker='amqp://guest@localhost//')

@app.task
def download():
	return filename 

@app.task
def addtoSQL():
	import sqlite3 as lite
	import busparse
	
	try:
	    conn = lite.connect('E:/Kaggle/Muni/munipileup/newavl.db')
	    c = conn.cursor()
	    c.execute("""CREATE TABLE avl (REV int, REPORT_TIME timestamp, VEHICLE_TAG text, LONGITUDE real, LATITUDE real, SPEED real,
	              HEADING real, TRAIN_ASSIGNMENT text, PREDICTABLE int)""")
	    conn.commit()
	    conn.close()
	
	except:
	    files = glob.glob('E:/Kaggle/Muni/munipileup/static/data/historical/sfmta*.csv')
	    for f in files:
	        ap = busparse.AvlParse()
	        ap.makeframe(busAVL=f)
	        ap.addSQL()