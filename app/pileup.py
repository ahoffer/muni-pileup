import urllib2
from datetime import datetime, timedelta
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify
from flask.ext.sqlalchemy import SQLAlchemy

# create our little application :)
app = Flask(__name__)
#app.config.from_object(__name__)
#app.config.from_pyfile(config.py)
#db = SQLAlchemy(app)

@app.route('/')
def layout():
    return render_template('layout.html')

@app.route('/historical')
def historical():
    import sqlite3 as lite
    from datetime import datetime as dt
    import time

    starttime = time.clock()

    conn = lite.connect('newavl.db')
    c = conn.cursor()
    c.execute("""SELECT LATITUDE, LONGITUDE FROM avl WHERE REPORT_TIME BETWEEN (?) AND (?) AND SPEED<20""", (dt(2013, 1, 1, 1, 0), dt(2013, 1, 2, 2, 0)))
    querytime = time.clock() - starttime
    heatelements = [list(row) for row in c.fetchall()]
    #OLD LINE #heatelements = [list(ts.values[x]) for (x,y), value in np.ndenumerate(ts.values)]

    conn.close()

    elapsedtime = time.clock() - starttime
    print(elapsedtime)
    return render_template('historical.html', heatdata=heatelements, size=len(heatelements), time=querytime)

# @app.route('/speedmap')
# def speed():

#     return render_template('speedmap.html')

@app.route('/realtime')
def realtime():
    import nextbus as nb
    #from nextbus import realtime as nexttask
    (coords, routes) = nb.realtime()

    #(coords, routes) = nexttask.delay()
    # import sqlite3 as lite
    # conn = lite.connect('newavl.db')
    # c = conn.cursor()
    # c.execute("SELECT * FROM nextbus ORDER BY ROWID DESC LIMIT 1")
    # data = c.fetchone()


    return render_template('realtime.html', coords=coords, routes=routes)

@app.route('/realbus', methods=['GET'])
def realbus():
    import nextbus as nb

    if request.method == 'GET':
         busid = request.args.getlist('bus_id')
         print(busid)
         (coords, routes) = nb.realtime(busnumber=int(busid[0]))
    else:
         (coords, routes) = nb.realtime()

    return render_template('realtime.html', coords=coords, routes=routes)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404

# @app.route('/data/api/<int:bus_id>', methods = ['GET'])
# def nextbus_proxy(bus_id):
#     if request.method == 'GET':
#         epoch = datetime(1970,1,1)
#         dt = datetime.utcnow() - epoch
#         dt_seconds = dt.seconds + dt.days * 24 * 3600
#         url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=sf-muni&r='+str(bus_id)+'&t='+str(dt_seconds)

#         data = urllib2.urlopen(url).read()
        
#         #return jsonify(data)
#         return jsonify( { 'bus': 5})
#     else:
#         return jsonify( {'bus': 6})

if __name__ == '__main__':
    app.run(debug = True)