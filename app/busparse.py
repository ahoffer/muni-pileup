#data from ftp://75.10.224.35/AVL_DATA/AVL_RAW/
import pandas as pd
import numpy as np
from datetime import datetime, timedelta

class AvlParse:
    #def __init__(self):

    def makeframe(self, busAVL="E:/Kaggle/Muni/munipileup/testdata.csv"):
        self.muniframe = pd.read_csv(busAVL, skipinitialspace=True)
        self.muniframe = self.muniframe[np.isfinite(self.muniframe['LATITUDE'])]
        self.muniframe = self.muniframe[(self.muniframe['LATITUDE'] < 40.0) & (self.muniframe['LATITUDE'] > 35.0)]
        self.muniframe = self.muniframe[(self.muniframe['LONGITUDE'] > -125.0) & (self.muniframe['LONGITUDE'] < -120.0)]
        self.muniframe["REPORT_TIME"] = pd.to_datetime(self.muniframe["REPORT_TIME"])
        self.muniframe.set_index('REPORT_TIME', drop=False, inplace=True)
        self.muniframe = self.muniframe.sort()  # Need to sort before splicing in pandas
        
        #remove buses that don't appear frequently
        return self.muniframe

    def vehicle_filter(self, tag='T504'):
        singlebus = self.muniframe[self.muniframe['VEHICLE_TAG'] == tag]
        ts = singlebus.ix[:, ['LATITUDE', 'LONGITUDE']]
        heatelements = [list(ts.values[x]) for (x,y), value in np.ndenumerate(ts.values)]
    
        return heatelements
    
    def time_filter(self, starttime=datetime(2014, 1, 5, 0), endtime=datetime(2014, 1, 5, 1)):
        timerange = self.muniframe.ix[starttime:endtime]
        ts = timerange.ix[:, ['LATITUDE', 'LONGITUDE']]
        heatelements = [list(ts.values[x]) for (x,y), value in np.ndenumerate(ts.values)]
    
        return heatelements

    def addSQL(self):
        import sqlite3 as lite 
        
        # c.execute("""CREATE TABLE avl (REV int, REPORT_TIME timestamp, VEHICLE_TAG text, LONGITUDE real, LATITUDE real, SPEED real,
        #            HEADING real, TRAIN_ASSIGNMENT text, PREDICTABLE int)""")
        conn = lite.connect('newavl.db')
        c = conn.cursor()
        framearray = self.muniframe.values

        framelist = [(element[0], element[1].strftime('%Y-%m-%d %H:%M:%S'), str(element[2]),
                      element[3], element[4], element[5], element[6], str(element[7]), element[8],) for element in framearray]
        c.executemany("INSERT INTO avl VALUES(?,?,?,?,?,?,?,?,?)", framelist)
        conn.commit()
        conn.close()
        print("Frame added to database.")

    def getSQL(self, command):
        import sqlite3 as lite
        from pandas.io import sql

        conn = lite.connect('newavl.db')

        conn.commit()
        conn.close()

        return dataframe