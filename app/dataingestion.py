# one time use file to get route data
# based on http://inventwithpython.com/blog/2014/02/11/lets-create-software-bus-routes-overlaid-on-google-maps/
import urllib2
import xml.etree.ElementTree as ET
import json
def get_routes():
	url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=sf-muni'
	data = urllib2.urlopen(url).read()
	root = ET.fromstring(data)
	routes = []
	for child in root:
		routes.append(child.attrib['tag'])


	route_paths = []
	for route in routes:
		if route.isdigit(): #only look at the numbered bus routes
			url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni&r='+route
			data = urllib2.urlopen(url).read()
			root = ET.fromstring(data)
			
			for child in root[0]:
				if child.tag != 'path':
					continue
				
				path_element = []
				route_path = {"type": "Feature",
					  "properties": {
					  		},
					  "geometry": {
					  		"type": "LineString"
					  		}
					 }

				for point in child:
					path_element.append([float(point.attrib['lon']), float(point.attrib['lat'])])
			
				route_path["properties"]["name"] = route
				route_path["geometry"]["coordinates"] = path_element
				route_paths.append(route_path)

	f = open('E:/Kaggle/Muni/munipileup/routes.json', 'w')
	
	f.write(json.dumps(route_paths))
	f.close()