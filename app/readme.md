#Welcome to MUNI Pileup
--------------------------
The goal is to try and make MUNI bus spacing more efficient such that buses are more optimally spaced to cut down on very long wait times. 
This is two separate projects sort of smashed together.

##Real time
-------------
We use NextBus API to get realtime bus position data



##Historical
-----------------------------
Use the AVL from last two years for SF muni to get historical data to look for trends and help improve real time predictions.


