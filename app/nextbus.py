from celery import Celery
app = Celery('nextbus', backend='amqp', broker='amqp://guest@localhost//')

@app.task
def realtime(busnumber=None):
    from datetime import datetime
    import urllib2
    import sqlite3 as lite
    import xml.etree.ElementTree as ET
    import geojson
    from nextbus import haversine

    conn = lite.connect("avl.db")
    c = conn.cursor()

    url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=sf-muni'
    data = urllib2.urlopen(url).read()
    root = ET.fromstring(data)
    buslist = []
    for child in root:
        if child.attrib['tag'].isdigit():
            buslist.append(child.attrib['tag'])

    epoch = datetime(1970,1,1)
    dt = datetime.utcnow() - epoch
    dt_seconds = dt.seconds + dt.days * 24 * 3600

    allbuses = {}

    allcoords = []
    distances = {}

    if busnumber:
        buslist = [busnumber,]
    for bus_id in buslist:
        coords = {
            "type": "Feature",
            "geometry":{

            "type": "MultiPoint",
            "coordinates": []
            },
            "properties":{}
        }
        url = 'http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=sf-muni&r='+str(bus_id)+'&t='+str(dt_seconds)
        data = urllib2.urlopen(url).read()
        root = ET.fromstring(data)
        currentbuses = []
        for child in root:
            if child.tag == 'vehicle':
                currentbuses.append({key: child.attrib[key] for key in child.keys()})

        allbuses[bus_id] = currentbuses
        if not currentbuses:
            print("No buses for route: "+str(bus_id))
            continue
        try:
            for bus in currentbuses:
                coords["geometry"]["coordinates"].append([float(bus['lon']), float(bus['lat'])])
        except:
            print('Did not work')  # not all buses have the same data string, varies in length from 7-9
            for bus in currentbuses:
                print(bus)
        if len(currentbuses) == 1:
            coords["geometry"]["type"] = "Point"
            coords["geometry"]["coordinates"] = [float(currentbuses[0]['lon']), float(currentbuses[0]['lat'])]

        latlng = [[float(bus['lon']), float(bus['lat'])] for bus in currentbuses]
        #havdist = haversine(latlng) # need to convert haversine to take array
        coords["properties"]["bus"] = bus_id
        allcoords.append(coords)
        #distances[bus_id] = dist

    geojsoncoords = {
                        "type": "FeatureCollection"
                    }

    geojsoncoords["features"] = allcoords

    #nextbusdata = 
    #c.executemany('INSERT INTO nextbus VALUES (?,?,?,?,?,?)', nextbusdata)
    #conn.commit()
    conn.close()
    return (geojson.dumps(geojsoncoords), buslist)

def snapto():
    import geojson

    f = open('../static/routes.json', 'r')
    routes = f.read()
    routelist = geojson.loads(routes)

def haversine(lon1, lat1, lon2, lat2):
    from math import radians, cos, sin, asin, sqrt

    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 

    # 6367 km is the radius of the Earth
    km = 6367 * c
    return km 
