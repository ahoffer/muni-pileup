  var map = L.map('map').setView([37.7833, -122.41941550], 14);
  L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png', {
    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>.',
    maxZoom: 15, 
    minZoom: 13
}).addTo(map);

var busIcon = L.icon({
	iconUrl: '../static/icons/bus3.png', 
	iconSize: [20, 20],
	iconAnchor: [10, 10]
});

L.marker([37.7833, -122.41941550], {icon: busIcon}).addTo(map);

$(document).ready(function(){
    $.getJSON('static/routes.json', function(data){
        L.geoJson(data).addTo(map);
    
    });
});

// var data = {bus: 5}
// $.ajax({
//             type: 'GET',
//             url: 'http://localhost:5000/data/api/',
//             data: data,
//             error: function(request, error) {
//                 console.log(arguments);
//  				alert ( " Can't do because: " + error );
//             },
//             success: function(data) {
//             	L.marker([37.781, -122.41941550], {icon: busIcon}).addTo(map);
//             }

//         });
